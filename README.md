This project shows statistics results for queries on the staff and students of the University of Crete extracting 
data from ldap.

Specifically, the 1st query extracts statistics such as the allocation of undergraduate and postgraduate students per 
School for the 17 Schools and the active, alumni and total number of students. 
The 2nd query extracts statistics such as the percentages of active and inactive staff per category of employees for the 4 
categories (A, B, Elke and the rest) as well as the absolute numbers. 
The 3rd query regarding Var accounts shows the percentages of Var accounts per School.
Finally, the 4th query regarding total statistics shows the active and inactive staff per total, the active 
and alumni students per total as well as the absolute numbers. 

The user can convert the results to Pdf and download it locally. The results are renewed every hour from the Database (DB). 
SESSION variables are used so that the DB credentials are accessible across all Php files.

It is written in Php, it uses Jpgraph library for Php-driven charts and Dompdf to derive the Pdf of the results. 
MySql Database is used.

It is written for the Ucnet (Unit of Communication Networking) of the University of Crete (Uoc).