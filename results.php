<html>
<body>

<?php
	if(isset($_POST['searchtype']))
	{
		$varQuery = $_POST['searchtype'];
				
		switch($varQuery)
		{
			case "students": $redir = "students.php"; break;
			case "staff": $redir = "staff.php"; break;
			case "accountsVar": $redir = "accountsVar.php"; break;
			case "total": $redir = "total.php"; break;
			default: echo("Error!"); exit(); break;
		}

		echo " redirecting to: $redir ";
		header("Location: $redir");
		// end method 1
		// method 2: dynamic redirect
		//header("Location: " . $varCountry . ".html");
		// end method 2
				
		exit();
	}	
?>

</body>
</html>