<?php // content="text/plain; charset=utf-8"

	require("page.php");
	require_once ('jpgraph/src/jpgraph.php');
	require_once ('jpgraph/src/jpgraph_pie.php');
	require_once ('jpgraph/src/jpgraph_pie3d.php');
	
	//start session
	session_start();
	
	/* access DB */	
	$host = $_SESSION['host'];
	$username = $_SESSION['username'];
	$password = $_SESSION['password'];
	$dbName = $_SESSION['dbName'];
	
	@$db = new mysqli($host, $username, $password, $dbName);
	
	if (mysqli_connect_errno()) {
       echo '<p>Error: Could not connect to database.<br/>
       Please try again later.</p>';
       exit;
    }
	
    $query = "SELECT InactiveA, InactiveB, InactiveE, InactiveRest, InactiveTotal FROM Staff 
			  ORDER BY Ts DESC LIMIT 1";
    $stmt = $db->prepare($query); 
    $stmt->execute();
    $stmt->store_result();
  
    $stmt->bind_result($inactiveA, $inactiveB, $inactiveE, $inactiveRest, $inactiveTotal);
    while($stmt->fetch()) {
	  $inactiveA; 
	  $inactiveB; 
	  $inactiveE; 
	  $inactiveRest;
      $inactiveTotal; 
    }

    $stmt->free_result();
    $db->close();
	
	//percentages for pie 3
	$inactiveA = round(($inactiveA / $inactiveTotal)*100);
	$inactiveB = round(($inactiveB / $inactiveTotal)*100);
	$inactiveE = round(($inactiveE / $inactiveTotal)*100);
	$inactiveRest = round(($inactiveRest / $inactiveTotal)*100);

	// Some data
	$data = array($inactiveA, $inactiveB, $inactiveE, $inactiveRest);

	// Create the Pie Graph. 
	$graph = new PieGraph(550,350);

	$theme_class= new UniversalTheme;
	$graph->SetTheme($theme_class);

	// Set A title for the plot
	$graph->title->Set("Μη Ενεργό Προσωπικό Ανά Κατηγορία");
	$graph->title->SetFont(FF_VERDANA,FS_BOLD,14);
	
	// Create
	$p1 = new PiePlot3D($data);
	$graph->Add($p1);

	$p1->ShowBorder();
	$p1->SetColor('black');
	$p1->SetSliceColors(array('#34387B','#A03451','#2E8E2E','#5E2A76'));
	$p1->ExplodeAll(10);
	$p1->value->SetFont(FF_FONT1,FS_BOLD,5);
	$p1->value->SetColor('#FFFEF3');
	$p1->SetLabels($data,0.5);
	
	$p1->SetLegends(array("A' Προσωπικό","B' Προσωπικό","ΕΛΚΕ","Λοιποί"));
	$graph->legend->SetAbsPos(0,315,'right','center');
	$graph->legend->SetColumns(1);

	$gdImgHandler = $graph->Stroke(_IMG_HANDLER);

	$fileName = "tmp/inactive_staff_pie.png";
	$graph->img->Stream($fileName);
 
	// Send it back to browser
	$graph->img->Headers();
	$graph->img->Stream();
?>