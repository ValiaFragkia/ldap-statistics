<?php
	// Include autoloader
	require_once "dompdf/autoload.inc.php";
	
	//start session
	session_start();
	
	use Dompdf\Dompdf;
	
	/* access DB */	
	$host = $_SESSION['host'];
	$username = $_SESSION['username'];
	$password = $_SESSION['password'];
	$dbName = $_SESSION['dbName'];
	
	@$db = new mysqli($host, $username, $password, $dbName);
	
	if (mysqli_connect_errno()) {
		echo '<p>Error: Could not connect to database.<br/>
			     Please try again later.</p>';
		exit;
	}
	
	$query = "SELECT ActiveTotal, InactiveTotal, Total,
					 DATE_FORMAT(Ts, '%m-%d-%Y %H:%i') 
			  FROM Staff ORDER BY Ts DESC LIMIT 1";
	$stmt = $db->prepare($query);
    //   $stmt->bind_param('i', $id);  
	$stmt->execute();
	$stmt->store_result();
  
	$stmt->bind_result($activeTotal, $inactiveTotal, $total, $ts);

	while($stmt->fetch()) {
		$activeTotal;
		$inactiveTotal;
		$total;
		$ts;
	}

	$stmt->free_result();
	$db->close();
			
	$activePerc = round(($activeTotal/$total)*100);
	$inactivePerc = round(($inactiveTotal/$total)*100);	
	
	//create pdf
	$dompdf = new Dompdf();

	$dompdf->loadHtml("<head>
						<title>Στατιστικά Κεντρικής Υπηρεσίας Καταλόγου</title>
						<link href='css/stylesPdf.css' type='text/css' rel='stylesheet'>
					   </head><header class='header-class'>
						<meta charset='utf-8'>
						</header>
						<h2>Προσωπικό</h2>
						<table id='table_date'>
							<tr>
								<th>Ημερομηνία</th>
								<td id='cell'>".$ts."</td>
							</tr>
						</table>
						<table>
							<tr>
								<th>Ενεργό προσωπικό</th>
								<td class='bold-text'>".$activeTotal."</td>
								<td class='bold-text'>".$activePerc."%</td>
							</tr>
							<tr>
								<th>Μη ενεργό προσωπικό</th>
								<td class='bold-text'>".$inactiveTotal."</td>
								<td class='bold-text'>".$inactivePerc."%</td>
							</tr>
							<tr>
								<th>Σύνολο προσωπικού</th>
								<td class='bold-text'>".$total."</td>
								<td class='bold-text'>100%</td>
							</tr>
						</table>
						<br />
						<br />
						<img src='tmp/total_staff_pie.png' alt='total_staff_pie'></img>
						<img src='tmp/active_staff_pie.png' alt='active_staff_pie'></img>
						<img src='tmp/inactive_staff_pie.png' alt='inactive_staff_pie'></img>");
	
	$dompdf->setPaper('A4');
	
	$dompdf->render();
	
	$dompdf->stream("", array("Attachment" =>0));
	
?>