<?php
	ini_set('memory_limit','512M');
	ini_set('max_execution_time', 900); 
	class Page
{
	public $graphs;
	public $table1;
	public $table2;
	public $btn;
	public $title = "UOC LDAP Statistics";
	
	 // class Page's operations
	public function __set($name, $value)
	{
		$this->$name = $value;
	}
  
	public function Display()
    {
		echo "<html>\n<head>\n";
		$this -> DisplayTitle();
		$this -> DisplayStyles();
		echo "</head>\n<body>\n";
		$this -> DisplayHeader();
		$this -> DisplayForm();
		echo $this->table1;
		echo $this->graphs;
		echo $this->table2;
		echo $this->btn;
		$this -> DisplayFooter();
		echo "</body>\n</html>\n";
	}
	
	public function DisplayTitle()
	{
		echo "<title>".$this->title."</title>";
	}

	public function DisplayStyles()
	{	 
		?>   
		<link href="css/styles.css" type="text/css" rel="stylesheet">
		<?php
	}

	public function DisplayHeader()
	{ 
		?>   
		<div id="page">
			<!-- page header -->
			<header>
				<meta charset="utf-8">
				<img id="image-align-left" src="uoc_logo.png" alt="uoc logo" height="90" width="90" align = "left"/> 
				<h2>ΠΑΝΕΠΙΣΤΗΜΙΟ ΚΡΗΤΗΣ</h2>
				<img id="image-align-right" src="ucnet_logo.png" alt="uoc logo" height="100" width="240"/> 
			</header><!-- end header -->
		<?php
	}
	
	public function DisplayForm()
	{
		?>
		<div id="content">
			<form action="results.php" method="post">
				<p id='choice'><strong>Επιλέξτε προς Αναζήτηση:</strong></p><br />
					<select id="selection" name="searchtype">
						<option value="students">Στοιχεία προπτυχιακών και μεταπτυχιακών φοιτητών</option>
						<option value="staff">Στοιχεία προσωπικού</option>
						<option value="accountsVar">Λογαριασμοί Var</option>
						<option value="total">Συγκεντρωτικά στατιστικά</option>
					</select> <br />
				<input type="submit" name="submit" value="Search">
			</form>
		<?php
	}
	
	public function DisplayFooter()
	{
		?>
			</div>
		</div>
		<footer>
			<div id="copyright-align-left">
				<p>© Copyright 2018 ucnet.uoc.gr</p>
			</div>
			<div id="copyright-align-right">
				<a href="http://www.uoc.gr/" target="_blank"><img alt="uoc logo small" src="uoc_logo_small.png" width="70" height="70"></a>
			</div>
		</footer>
		<?php
	}
}
?>