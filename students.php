<?php
	/* Ldap is accessed only in the staff.php and writes to DB 
		and next scripts read data from DB. */
	
	require("page.php");
	
	//start session
	session_start();
	
	/* function to count ugrads */
	function countUgrads($school) {
	
		/* connect to ldap */
		$ldap_host = 'ds.uoc.gr';
		$ldap_port = 409;
	
		$ds = ldap_connect($ldap_host, $ldap_port)
				or die("Could not connect to LDAP server.");

		$bd = ldap_bind($ds) 
				or die ("System couldn't bind the connection!");
	
		$dn = "ou=ugrads,ou=".$school.",dc=uoc,dc=gr";
		$filter = "(&(uid=*))";
	
		$search = ldap_search($ds, $dn, $filter);  //result
		$result = ldap_get_entries($ds, $search);	//get result
		$ugrads = $result["count"];
	
		$dn = "ou=alumni,ou=ugrads,ou=".$school.",dc=uoc,dc=gr";
		$filter = "(&(uid=*))";
	
		$search = ldap_search($ds, $dn, $filter);  //result
		$result = ldap_get_entries($ds, $search);	//get result
		$alumni = $result["count"];
		$ugradsActive = $ugrads - $alumni;
		
		return array($ugrads, $ugradsActive);
	}
	
	 /*  function to count pgrads active and alumni for universities 
	    Pgrads are counted according to ldap scheme for each university */
	function countPgrads ($school, $alumni, $programsNum, $program1, $program2, 
						  $program3, $program4, $program5, $program6) {
		/* connect to ldap */
		$ldap_host = 'ds.uoc.gr';
		$ldap_port = 409;
	
		$ds = ldap_connect($ldap_host, $ldap_port)
				or die("Could not connect to LDAP server.");

		$bd = ldap_bind($ds) 
				or die ("System couldn't bind the connection!");
		
		if (!$alumni) {
			$dn = "ou=pgrads,ou=".$school.",dc=uoc,dc=gr";
			$filter = "(&(uid=*))";
	
			$search = ldap_search($ds, $dn, $filter);  
			$result = ldap_get_entries($ds, $search);	
			$pgradsActive = $result["count"];
			
			return array($pgradsActive, $pgradsActive); //it's the same, 0 alumni
		} else if ($programsNum == 0){ //no program
			$dn = "ou=pgrads,ou=".$school.",dc=uoc,dc=gr";
			$filter = "(&(uid=*))";
	
			$search = ldap_search($ds, $dn, $filter);  
			$result = ldap_get_entries($ds, $search);	
			$pgrads = $result["count"];
		
			$dn = "ou=alumni,ou=pgrads,ou=".$school.",dc=uoc,dc=gr";
			$filter = "(&(uid=*))";
	
			$search = ldap_search($ds, $dn, $filter);  
			$result = ldap_get_entries($ds, $search);	
			$alumni = $result["count"];
			$pgradsActive = $pgrads - $alumni;
	
			return array($pgrads, $pgradsActive);
		} else if ($programsNum == 1){ 
			$dn = "ou=".$program1.",ou=pgrads,ou=".$school.",dc=uoc,dc=gr";
			$filter = "(&(uid=*))";
	
			$search = ldap_search($ds, $dn, $filter);  
			$result = ldap_get_entries($ds, $search);	
			$pgrads = $result["count"];
			
			$dn = "ou=alumni,ou=".$program1.",ou=pgrads,ou=".$school.",dc=uoc,dc=gr";
			$filter = "(&(uid=*))";
	
			$search = ldap_search($ds, $dn, $filter);  
			$result = ldap_get_entries($ds, $search);	
			$alumni = $result["count"];
			$pgradsActive = $pgrads - $alumni;
			
			return array($pgrads, $pgradsActive);
		} else if ($programsNum == 2) {
			$dn = "ou=".$program1.",ou=pgrads,ou=".$school.",dc=uoc,dc=gr";
			$filter = "(&(uid=*))";
	
			$search = ldap_search($ds, $dn, $filter);  
			$result = ldap_get_entries($ds, $search);	
			$pgrads1 = $result["count"];
			
			$dn = "ou=alumni,ou=".$program1.",ou=pgrads,ou=".$school.",dc=uoc,dc=gr";
			$filter = "(&(uid=*))";
	
			$search = ldap_search($ds, $dn, $filter);  
			$result = ldap_get_entries($ds, $search);	
			$alumni1 = $result["count"];
			$pgradsActive1 = $pgrads1 - $alumni1;  //pgrads1
			
			$dn = "ou=".$program2.",ou=pgrads,ou=".$school.",dc=uoc,dc=gr";
			$filter = "(&(uid=*))";
	
			$search = ldap_search($ds, $dn, $filter);  
			$result = ldap_get_entries($ds, $search);	
			$pgrads2 = $result["count"];
			
			$dn = "ou=alumni,ou=".$program2.",ou=pgrads,ou=".$school.",dc=uoc,dc=gr";
			$filter = "(&(uid=*))";
	
			$search = ldap_search($ds, $dn, $filter);  
			$result = ldap_get_entries($ds, $search);	
			$alumni2 = $result["count"];
			$pgradsActive2 = $pgrads2 - $alumni2;  //pgrads2
		
			$pgrads = $pgrads1 + $pgrads2;
			$pgradsActive = $pgradsActive1 + $pgradsActive2;
			
			return array($pgrads, $pgradsActive);
		} else if ($programsNum == 4) {
			$dn = "ou=".$program1.",ou=pgrads,ou=".$school.",dc=uoc,dc=gr";
			$filter = "(&(uid=*))";
	
			$search = ldap_search($ds, $dn, $filter);  
			$result = ldap_get_entries($ds, $search);	
			$pgrads1 = $result["count"];
			
			$dn = "ou=alumni,ou=".$program1.",ou=pgrads,ou=".$school.",dc=uoc,dc=gr";
			$filter = "(&(uid=*))";
	
			$search = ldap_search($ds, $dn, $filter);  
			$result = ldap_get_entries($ds, $search);	
			$alumni1 = $result["count"];
			$pgradsActive1 = $pgrads1 - $alumni1;  //pgrads1
		
			$dn = "ou=".$program2.",ou=pgrads,ou=".$school.",dc=uoc,dc=gr"; //no alumni
			$filter = "(&(uid=*))";
	
			$search = ldap_search($ds, $dn, $filter);  
			$result = ldap_get_entries($ds, $search);	
			$pgrads2 = $result["count"];
		
			$dn = "ou=".$program3.",ou=pgrads,ou=".$school.",dc=uoc,dc=gr"; //no alumni
			$filter = "(&(uid=*))";
	
			$search = ldap_search($ds, $dn, $filter);  
			$result = ldap_get_entries($ds, $search);	
			$pgrads3 = $result["count"];
			
			$dn = "ou=".$program4.",ou=pgrads,ou=".$school.",dc=uoc,dc=gr";
			$filter = "(&(uid=*))";
	
			$search = ldap_search($ds, $dn, $filter);  
			$result = ldap_get_entries($ds, $search);	
			$pgrads4 = $result["count"];
			
			$dn = "ou=alumni,ou=".$program4.",ou=pgrads,ou=".$school.",dc=uoc,dc=gr";
			$filter = "(&(uid=*))";
	
			$search = ldap_search($ds, $dn, $filter);  
			$result = ldap_get_entries($ds, $search);	
			$alumni4 = $result["count"];
			$pgradsActive4 = $pgrads4 - $alumni4;  //pgrads4
				
			$pgrads = $pgrads1 + $pgrads2 + $pgrads3 + $pgrads4;
			$pgradsActive = $pgradsActive1 + $pgrads2 + $pgrads3 + $pgradsActive4;
			
			return array($pgrads, $pgradsActive);
		} else{ //med school - 6 programs
			$dn = "ou=".$program1.",ou=pgrads,ou=".$school.",dc=uoc,dc=gr";
			$filter = "(&(uid=*))";
	
			$search = ldap_search($ds, $dn, $filter);  
			$result = ldap_get_entries($ds, $search);	
			$pgrads1 = $result["count"];
			
			$dn = "ou=alumni,ou=".$program1.",ou=pgrads,ou=".$school.",dc=uoc,dc=gr";
			$filter = "(&(uid=*))";
	
			$search = ldap_search($ds, $dn, $filter);  
			$result = ldap_get_entries($ds, $search);	
			$alumni1 = $result["count"];
			$pgradsActive1 = $pgrads1 - $alumni1;  //pgrads1
			
			$dn = "ou=".$program2.",ou=pgrads,ou=".$school.",dc=uoc,dc=gr";
			$filter = "(&(uid=*))";
	
			$search = ldap_search($ds, $dn, $filter);  
			$result = ldap_get_entries($ds, $search);	
			$pgrads2 = $result["count"];
			
			$dn = "ou=alumni,ou=".$program2.",ou=pgrads,ou=".$school.",dc=uoc,dc=gr";
			$filter = "(&(uid=*))";
	
			$search = ldap_search($ds, $dn, $filter);  
			$result = ldap_get_entries($ds, $search);	
			$alumni2 = $result["count"];
			$pgradsActive2 = $pgrads2 - $alumni2;  //pgrads2
		
			$dn = "ou=".$program3.",ou=pgrads,ou=".$school.",dc=uoc,dc=gr";
			$filter = "(&(uid=*))";
	
			$search = ldap_search($ds, $dn, $filter);  
			$result = ldap_get_entries($ds, $search);	
			$pgrads3 = $result["count"];
			
			$dn = "ou=alumni,ou=".$program3.",ou=pgrads,ou=".$school.",dc=uoc,dc=gr";
			$filter = "(&(uid=*))";
	
			$search = ldap_search($ds, $dn, $filter);  
			$result = ldap_get_entries($ds, $search);	
			$alumni3 = $result["count"];
			$pgradsActive3 = $pgrads3 - $alumni3;  //pgrads3
			
			$dn = "ou=".$program4.",ou=pgrads,ou=".$school.",dc=uoc,dc=gr";
			$filter = "(&(uid=*))";
	
			$search = ldap_search($ds, $dn, $filter);  
			$result = ldap_get_entries($ds, $search);	
			$pgrads4 = $result["count"];
			
			$dn = "ou=alumni,ou=".$program4.",ou=pgrads,ou=".$school.",dc=uoc,dc=gr";
			$filter = "(&(uid=*))";
	
			$search = ldap_search($ds, $dn, $filter); 
			$result = ldap_get_entries($ds, $search);	
			$alumni4 = $result["count"];
			$pgradsActive4 = $pgrads4 - $alumni4;  //pgrads4
			
			$dn = "ou=".$program5.",ou=pgrads,ou=".$school.",dc=uoc,dc=gr";
			$filter = "(&(uid=*))";
	
			$search = ldap_search($ds, $dn, $filter);  
			$result = ldap_get_entries($ds, $search);	
			$pgrads5 = $result["count"];
			
			$dn = "ou=alumni,ou=".$program5.",ou=pgrads,ou=".$school.",dc=uoc,dc=gr";
			$filter = "(&(uid=*))";
	
			$search = ldap_search($ds, $dn, $filter);  
			$result = ldap_get_entries($ds, $search);	
			$alumni5 = $result["count"];
			$pgradsActive5 = $pgrads5 - $alumni5;  //pgrads5
			
			$dn = "ou=".$program6.",ou=pgrads,ou=".$school.",dc=uoc,dc=gr";
			$filter = "(&(uid=*))";
	
			$search = ldap_search($ds, $dn, $filter);  
			$result = ldap_get_entries($ds, $search);	
			$pgrads6 = $result["count"];
			
			$dn = "ou=alumni,ou=".$program6.",ou=pgrads,ou=".$school.",dc=uoc,dc=gr";
			$filter = "(&(uid=*))";
	
			$search = ldap_search($ds, $dn, $filter);  
			$result = ldap_get_entries($ds, $search);	
			$alumni6 = $result["count"];
			$pgradsActive6 = $pgrads6 - $alumni6;  //pgrads6
		
			$pgrads = $pgrads1 + $pgrads2 + $pgrads3 + $pgrads4 + $pgrads5 + $pgrads6;
			$pgradsActive = $pgradsActive1 + $pgradsActive2 + $pgradsActive3 + 
							$pgradsActive4 + $pgradsActive5 + $pgradsActive6;
			
			return array($pgrads, $pgradsActive);
		}
	}
	
	/* access DB to read Timestamp*/	
	$host = $_SESSION['host'];
	$username = $_SESSION['username'];
	$password = $_SESSION['password'];
	$dbName = $_SESSION['dbName'];
	
	@$db = new mysqli($host, $username, $password, $dbName);
	
	if (mysqli_connect_errno()) {
       echo '<p>Error: Could not connect to database.<br/>
       Please try again later.</p>';
       exit;
    }
	
    $query = "SELECT UNIX_TIMESTAMP(Ts) FROM Pgrads ORDER BY Ts DESC LIMIT 1";
    $stmt = $db->prepare($query); 
    $stmt->execute();
    $stmt->store_result();
  
    $stmt->bind_result($ts);

    while($stmt->fetch()) {
		$ts; 
    } 
	$stmt->free_result();
    $db->close();
	
	$nowunix = time();
	
	$diff = $nowunix - $ts;
	$diff = floor($diff/(60*60));
	
	if ($diff < 1) { //if Ts is less than 1 hour back
		/* access DB to read ugrads */	
		@$db = new mysqli($host, $username, $password, $dbName);
	
		if (mysqli_connect_errno()) {
			echo '<p>Error: Could not connect to database.<br/>
					 Please try again later.</p>';
			exit;
		}
	
		$query = "SELECT  TotalActive, Alumni, Total FROM Ugrads ORDER BY Ts DESC LIMIT 1";
		$stmt = $db->prepare($query);  
		$stmt->execute();
		$stmt->store_result();
	
		$stmt->bind_result($totalActiveUgrads, $alumniUgrads, $totalUgrads);

		while($stmt->fetch()) {
			$totalActiveUgrads;
			$alumniUgrads;
			$totalUgrads;
		}

		$stmt->free_result();
		$db->close();
		
		/* access DB to read pgrads */	
		@$db = new mysqli($host, $username, $password, $dbName);
	
		if (mysqli_connect_errno()) {
			echo '<p>Error: Could not connect to database.<br/>
					 Please try again later.</p>';
			exit;
		}
	
		$query = "SELECT  TotalActive, Alumni, Total FROM Pgrads ORDER BY Ts DESC LIMIT 1";
		$stmt = $db->prepare($query);  
		$stmt->execute();
		$stmt->store_result();
	
		$stmt->bind_result($totalActivePgrads, $alumniPgrads, $totalPgrads);

		while($stmt->fetch()) {
			$totalActivePgrads;
			$alumniPgrads;
			$totalPgrads;
		}

		$stmt->free_result();
		$db->close();
		
		$total = $totalUgrads + $totalPgrads;
		$totalActive = $totalActiveUgrads + $totalActivePgrads;
		$totalAlumni = $alumniUgrads + $alumniPgrads;
		
	} else {
		//1. biology
		$biologyUgrads = countUgrads("biology");
		$biologyPgrads = countPgrads("biology", false, 0, "","","","","","");
	
		//2. chemistry
		$chemistryUgrads = countUgrads("chemistry");
		$chemistryPgrads = countPgrads("chemistry", true, 1, "chemp","","","","","");
			
		//3. csd
		$csdUgrads = countUgrads("csd");
		$csdPgrads = countPgrads("csd", false, 0, "","","","","","");
	
		//4. economics
		$econUgrads = countUgrads("econ");
		$econPgrads = countPgrads("econ", true, 2, "econ1p", "econp","","","","");
	
		//5. fks
		$fksUgrads = countUgrads("fks");
		$fksPgrads = countPgrads("fks", true, 2, "bioethics", "fksp","","","","");	
	
		//6. history-archaeology
		$histArchUgrads = countUgrads("hist-arch");
		$histArchPgrads = countPgrads("hist-arch", true, 1, "iap","","","","","");

		//7. materials
		$materialsUgrads = countUgrads("materials");
		$materialsPgrads = countPgrads("materials", false, 0, "","","","","","");
		
		//8. math
		$mathUgrads = countUgrads("math");
		$mathPgrads = countPgrads("math", true, 1, "mathp", "","","","","");
		
		//9. tem - Efarmosmenon Mathimatikon
		$temUgrads = countUgrads("tem");
		$temPgrads = countPgrads("tem", true, 1, "temp", "","","","","");
	
		//10. philology
		$philologyUgrads = countUgrads("philology");
		$philologyPgrads = countPgrads("philology", true, 1, "philp", "","","","","");
	
		//11. ptde - Dimotikis Ekpaideusis
		$ptdeUgrads = countUgrads("ptde");
		$ptdePgrads = countPgrads("ptde", true, 4, "ptde1p", "ptde2p", "ptde3p", "ptdep","","");

		//12. ptpe - Prosxolikis Ekpaideusis
		$ptpeUgrads = countUgrads("ptpe");
		$ptpePgrads = countPgrads("ptpe", true, 1, "ptpep", "","","","","");

		//13. social
		$socialUgrads = countUgrads("social");
		$socialPgrads = countPgrads("social", true, 1, "socp", "","","","","");
	
		//14. politikis epistimis
		$polUgrads = countUgrads("pol");
		$polPgrads = countPgrads("pol", true, 1, "polp", "","","","","");
	
		//15. physics
		$physicsUgrads = countUgrads("physics");
		$physicsPgrads = countPgrads("physics", false, 0, "","","","","","");
	
		//16. medical
		$medUgrads = countUgrads("med");
		$medPgrads = countPgrads("med", true, 6, "med1p", "med2p", "med3p", "med4p", 
							     "med5p", "medp");
	
		//17. psychology
		$psychologyUgrads = countUgrads("psychology");
		$psychologyPgrads = countPgrads("psychology", true, 1, "psyp", "","","","","");

		//total
	
		$totalUgrads = $biologyUgrads[0] + $chemistryUgrads[0] + $csdUgrads[0] + 
				   $econUgrads[0] + $fksUgrads[0] + $histArchUgrads[0] + $materialsUgrads[0] +
				   $mathUgrads[0] + $temUgrads[0] + $philologyUgrads[0] + $ptdeUgrads[0] + 
				   $ptpeUgrads[0] + $socialUgrads[0] + $polUgrads[0] + $physicsUgrads[0] + 
				   $medUgrads[0] + $psychologyUgrads[0];
	
		$totalUgradsActive = $biologyUgrads[1] + $chemistryUgrads[1] + $csdUgrads[1] + 
				         $econUgrads[1] + $fksUgrads[1] + $histArchUgrads[1] + 
						 $materialsUgrads[1] + $mathUgrads[1] + $temUgrads[1] + 
						 $philologyUgrads[1] + 	$ptdeUgrads[1] + $ptpeUgrads[1] + 
						 $socialUgrads[1] + $polUgrads[1] + $physicsUgrads[1] + $medUgrads[1] +
						 $psychologyUgrads[1];
	
		$totalPgrads = $biologyPgrads[0] + $chemistryPgrads[0] + $csdPgrads[0] + 
				   $econPgrads[0] + $fksPgrads[0] + $histArchPgrads[0] + $materialsPgrads[0] +				   
				   $mathPgrads[0] + $temPgrads[0] + $philologyPgrads[0] + $ptdePgrads[0] + 
				   $ptpePgrads[0] + $socialPgrads[0] + $polPgrads[0] + $physicsPgrads[0] + 
				   $medPgrads[0] + $psychologyPgrads[0];
	
		$totalPgradsActive = $biologyPgrads[1] + $chemistryPgrads[1] + $csdPgrads[1] + 
						 $econPgrads[1] + $fksPgrads[1] + $histArchPgrads[1] + 
						 $materialsPgrads[1] + $mathPgrads[1] + $temPgrads[1] + 
						 $philologyPgrads[1] + $ptdePgrads[1] + $ptpePgrads[1] + 
						 $socialPgrads[1] + $polPgrads[1] + $physicsPgrads[1] + 
						 $medPgrads[1] + $psychologyPgrads[1];
	
		$total = $totalUgrads + $totalPgrads;
		$totalActive = $totalUgradsActive + $totalPgradsActive;
		$alumniUgrads = $totalUgrads - $totalUgradsActive;
		$alumniPgrads = $totalPgrads - $totalPgradsActive;
		$totalAlumni = $alumniUgrads + $alumniPgrads;
	
		/* store data in DB */
		@$db = new mysqli($host, $username, $password, $dbName);

		if (mysqli_connect_errno()) {
			echo "<p>Error: Could not connect to database.<br/>
					 Please try again later.</p>";
			exit;
		}
	
		//1st query - ugrads
		$query = "INSERT INTO Ugrads VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
											 ?, ?, ?, ?, ?, CURRENT_TIMESTAMP)";
		$stmt = $db->prepare($query);
		$stmt->bind_param('iiiiiiiiiiiiiiiiiiii', $biologyUgrads[1], $chemistryUgrads[1], 
												  $csdUgrads[1], $econUgrads[1], $fksUgrads[1], 
												  $histArchUgrads[1], $materialsUgrads[1], 
												  $mathUgrads[1], $temUgrads[1], $philologyUgrads[1], 
												  $ptdeUgrads[1], $ptpeUgrads[1], $socialUgrads[1], 
												  $polUgrads[1], $physicsUgrads[1], $medUgrads[1], 
												  $psychologyUgrads[1], $totalUgradsActive,
												  $alumniUgrads, $totalUgrads);
		$stmt->execute();
	
		//2nd query - pgrads
		$query = "INSERT INTO Pgrads VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
											 ?, ?, ?, ?, ?, CURRENT_TIMESTAMP)";
		$stmt = $db->prepare($query);
		$stmt->bind_param('iiiiiiiiiiiiiiiiiiii', $biologyPgrads[1], $chemistryPgrads[1], 
												  $csdPgrads[1], $econPgrads[1], $fksPgrads[1], 
										   		  $histArchPgrads[1], $materialsPgrads[1], 
												  $mathPgrads[1], $temPgrads[1], $philologyPgrads[1], 
												  $ptdePgrads[1], $ptpePgrads[1], $socialPgrads[1], 
												  $polPgrads[1], $physicsPgrads[1], $medPgrads[1], 
												  $psychologyPgrads[1], $totalPgradsActive, 
												  $alumniPgrads, $totalPgrads);
		$stmt->execute(); 

		/*  if ($stmt->affected_rows > 0) {
			echo  "<p>Stats inserted into the database.</p>";
		} else {
			echo "<p>An error has occurred.<br/>
					 Stats were not added.</p>";
		}*/
	
		$db->close();
	}
	
	$activePerc = round(($totalActive/$total)*100);
	$alumniPerc = round(($totalAlumni/$total)*100);
	
	$studentsPage = new Page();
	$studentsPage->table1 ="<p id='res'>Αποτελέσματα</p>
							<p id='note'>Τα αποτελέσματα ανανεώνονται από τη Βάση Δεδομένων
							   κάθε μία ώρα.</p>
						<table>
							<tr>
								<th>Ενεργοί - αναστολή σπουδών φοιτητές</th>
								<td class='bold-text'>".$totalActive."</td>
								<td class='bold-text'>".$activePerc."%</td>
							</tr>
							<tr>
								<th>Μη ενεργοί φοιτητές</th>
								<td class='bold-text'>".$totalAlumni."</td>
								<td class='bold-text'>".$alumniPerc."%</td>
							</tr>
							<tr>
								<th>Σύνολο φοιτητών</th>
								<td class='bold-text'>".$total."</td>
								<td class='bold-text'>100%</td>
							</tr>
						</table>";
						
	$studentsPage->graphs ="<div class='images'>
							<p id='img1' class='graphs'> <img src='ugradsGraph.php?a=1&b=2' alt='ugrads_pie'> </p>
							<p id='img2' class='graphs'> <img src='pgradsGraph.php?a=1&b=2' alt='pgrads_pie'> </p>
						</div>";	
	$studentsPage->btn ="<form action='studentsPdf.php' method='get>
						<p class='buttons'><input class='btn' type='submit' name='submit' value='Download pdf'></p>
					 </form>";						
	$studentsPage->Display();
?>