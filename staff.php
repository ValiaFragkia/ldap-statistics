<?php
	/* Ldap is accessed only in the staff.php and writes to DB 
	and next scripts read data from DB. */
	
	require("page.php");
	
	//start session
	session_start();
	
	/* count A-, B-, E- employees categories for active employees function*/
	function countCategories($total, $data) {
		//counters
		$countA = 0;	
		$countB = 0;	
		$countE = 0;	
		$totalData = $data["count"];
	
		for ($i=0;$i<$totalData;$i++)
		{
			$employeeNum = $data[$i]["employeenumber"][0];
	
			//check greek and english chars
			$posA = strpos($employeeNum, 'Α-'); 
			$posB = strpos($employeeNum, 'Β-'); 
			$posE = strpos($employeeNum, 'Ε-');	
	
			if (!($posA === false))
			{
				$countA++;
			} else {
				$posA = strpos($employeeNum, 'A-');  
				if (!($posA === false))
				{
					$countA++;
				}		 
			}
			if (!($posB === false))
			{
				$countB++;
			} else {
				$posB = strpos($employeeNum, 'B-');  
				if (!($posB === false))
				{
					$countB++;
				} 
				
			}
			if (!($posE === false))
			{
				$countE++;
			} else {
				$posE = strpos($employeeNum, 'E-');  
				if (!($posE === false))
				{
					$countE++;
				}
			}
		}
		$rest = $total - ($countA+$countB+$countE);
		
		return array($countA, $countB, $countE, $rest);
	}
	
	/* access DB to read Timestamp*/	
	$host = $_SESSION['host'];
	$username = $_SESSION['username'];
	$password = $_SESSION['password'];
	$dbName = $_SESSION['dbName'];
	
	@$db = new mysqli($host, $username, $password, $dbName);
	
	if (mysqli_connect_errno()) {
       echo '<p>Error: Could not connect to database.<br/>
       Please try again later.</p>';
       exit;
    }
	
    $query = "SELECT UNIX_TIMESTAMP(Ts) FROM Staff ORDER BY Ts DESC LIMIT 1";
    $stmt = $db->prepare($query); 
    $stmt->execute();
    $stmt->store_result();
  
    $stmt->bind_result($ts);

    while($stmt->fetch()) {
		$ts; 
    } 
	$stmt->free_result();
    $db->close();
	
	$nowunix = time();
	
	$diff = $nowunix - $ts;
	$diff = floor($diff/(60*60));
	
	if ($diff < 1) { //if Ts is less than 1 hour back
		/* access DB */	
		@$db = new mysqli($host, $username, $password, $dbName);
	
		if (mysqli_connect_errno()) {
			echo '<p>Error: Could not connect to database.<br/>
			         Please try again later.</p>';
			exit;
		}
	
		$query = "SELECT ActiveTotal, InactiveTotal, Total FROM Staff ORDER BY Ts DESC LIMIT 1";
		$stmt = $db->prepare($query);
	    //   $stmt->bind_param('i', $id);  
		$stmt->execute();
		$stmt->store_result();
  
		$stmt->bind_result($activeTotal, $inactiveTotal, $total);

		while($stmt->fetch()) {
			$activeTotal;
			$inactiveTotal;
			$total;
		}

		$stmt->free_result();
		$db->close();
		
	} else {
		/* connect to ldap */
		$ldap_host = 'ds.uoc.gr';
		$ldap_port = 409;
		$dn = "ou=retired,ou=People,dc=uoc,dc=gr";
	
		$ds = ldap_connect($ldap_host, $ldap_port)
					or die("Could not connect to LDAP server.");

		$bd = ldap_bind($ds) 
					or die ("System couldn't bind the connection!");
		
		//retired employees
		$filter = "(&(uid=*))";
	
		$search = ldap_search($ds, $dn, $filter);  
		$result = ldap_get_entries($ds, $search);	
		$inactiveTotal = $result["count"];

		//inactive with employeeNumber
		$filter = "(&(employeeNumber=*))";
	
		$search = ldap_search($ds, $dn, $filter);  
		$result = ldap_get_entries($ds, $search);	

		$inactiveCounts = countCategories($inactiveTotal, $result);
	
		//active employees
		$dn = "ou=People,dc=uoc,dc=gr";
	
		$filter = "(&(uid=*))";
	
		$search = ldap_search($ds, $dn, $filter);  
		$result = ldap_get_entries($ds, $search);	
		$total = $result["count"];
	
		//active with employeeNumber
		$filter = "(&(employeeNumber=*))";
	
		$search = ldap_search($ds, $dn, $filter);  
		$result = ldap_get_entries($ds, $search);	
	
		$activeCounts = countCategories($total, $result);
	
		//fix because of the subtree retired 
		$activeCounts[0] -= $inactiveCounts[0];
		$activeCounts[1] -= $inactiveCounts[1];
		$activeCounts[2] -= $inactiveCounts[2];
		$activeCounts[3] -= $inactiveCounts[3];	
		$activeTotal = $total - $inactiveTotal;
		
		/*store data in DB*/
		@$db = new mysqli($host, $username, $password, $dbName);

		if (mysqli_connect_errno()) {
			echo "<p>Error: Could not connect to database.<br/>
					 Please try again later.</p>";
			exit;
		}

		$query = "INSERT INTO Staff VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP)";
		$stmt = $db->prepare($query);
		$stmt->bind_param('iiiiiiiiiii', $activeCounts[0], $activeCounts[1], $activeCounts[2], 
										 $activeCounts[3], $activeTotal, $inactiveCounts[0], 
										 $inactiveCounts[1], $inactiveCounts[2], 
										 $inactiveCounts[3], $inactiveTotal, $total);
		$stmt->execute();

	    /* if ($stmt->affected_rows > 0) {
			echo  "<p>Stats inserted into the database.</p>";
		} else {
			echo "<p>An error has occurred.<br/>
			     	 Stats were not added.</p>";
		}*/
  
		$db->close();
	}
	
	$activePerc = round(($activeTotal/$total)*100);
	$inactivePerc = round(($inactiveTotal/$total)*100);
	
	
	$staffPage = new Page();
	
	$staffPage->table1 ="<p id='res'>Αποτελέσματα</p>
						 <p id='note'>Τα αποτελέσματα ανανεώνονται από τη Βάση Δεδομένων
							   κάθε μία ώρα.</p>
						<table>
							<tr>
								<th>Ενεργό προσωπικό</th>
								<td class='bold-text'>".$activeTotal."</td>
								<td class='bold-text'>".$activePerc."%</td>
							</tr>
							<tr>
								<th>Μη ενεργό προσωπικό</th>
								<td class='bold-text'>".$inactiveTotal."</td>
								<td class='bold-text'>".$inactivePerc."%</td>
							</tr>
							<tr>
								<th>Σύνολο προσωπικού</th>
								<td class='bold-text'>".$total."</td>
								<td class='bold-text'>100%</td>
							</tr>
						</table>";	
	
	$staffPage->graphs ="<div class='images'>
						<p id='img3' class='graphs'> <img src ='totalStaff.php?' alt='pie_total'> </p>
						<p id='img4' class='graphs'> <img src ='inactiveStaff.php?' alt='pie_active'> </p>
						<p id='img5' class='graphs'> <img src ='activeStaff.php?' alt='pie_inactive'> </p>
						</div>";
	$staffPage->btn ="<form action='staffPdf.php' method='get>
						<p class='buttons'><input class='btn' type='submit' name='submit' value='Download pdf'></p>
					 </form>";							
	$staffPage->Display();
?>