<?php
	// Include autoloader
	require_once "dompdf/autoload.inc.php";
	
	//start session
	session_start();
	
	use Dompdf\Dompdf;
	
	/* access DB */	
	$host = $_SESSION['host'];
	$username = $_SESSION['username'];
	$password = $_SESSION['password'];
	$dbName = $_SESSION['dbName'];
	
	@$db = new mysqli($host, $username, $password, $dbName);
	
	if (mysqli_connect_errno()) {
		echo '<p>Error: Could not connect to database.<br/>
			     Please try again later.</p>';
		exit;
	}
	
	$query = "SELECT ActiveStudents, Alumni, TotalStudents, ActiveStaff,
					 InactiveStaff, TotalStaff, TotalActive, TotalInactive, 
					 TotalVar, Total, DATE_FORMAT(Ts, '%m-%d-%Y %H:%i') 
			  FROM TotalStatistics
			  ORDER BY Ts DESC LIMIT 1";
	$stmt = $db->prepare($query); 
	$stmt->execute();
	$stmt->store_result();
  
	$stmt->bind_result($activeStudents, $alumni, $totalStudents, $activeStaff, 
						$inactiveStaff, $totalStaff, $totalActive, $totalInactive, 
						$totalVar, $total, $ts);
	while($stmt->fetch()) {
		$totalActive;
		$activeStudents; 
		$alumni; 
		$totalStudents; 
		$activeStaff; 
		$inactiveStaff; 
		$totalStaff; 
		$totalActive; 
		$totalInactive; 
		$totalVar;
		$total;
		$ts;
	}

	$stmt->free_result();
	$db->close();
		
	$active = round(($totalActive / $total)*100);
	$inactive = round(($totalInactive / $total)*100);	
	
	//create pdf
	$dompdf = new Dompdf();

	$dompdf->loadHtml("<head>
						<title>Στατιστικά Κεντρικής Υπηρεσίας Καταλόγου</title>
						<link href='css/stylesPdf.css' type='text/css' rel='stylesheet'>
					   </head><header class='header-class'>
						<meta charset='utf-8'>
						</header>
						<h2>Συγκεντρωτικά Στοιχεία</h2>
						<table id='table_date'>
							<tr>
								<th>Ημερομηνία</th>
								<td id='cell'>".$ts."</td>
							</tr>
						</table>
							<table>
							<tr>
								<th></th>
								<th>Ενεργoί</th>
								<th>Μη Ενεργoί</th>
								<th>Σύνολο</th>
							</tr>
							<tr>
								<th>Φοιτητές</th>
								<td>".$activeStudents."</td>
								<td>".$alumni."</td>
								<td>".$totalStudents."</td>
							</tr>
							<tr>
								<th>Προσωπικό</th>
								<td>".$activeStaff."</td>
								<td>".$inactiveStaff."</td>
								<td>".$totalStaff."</td>
							</tr>
							<tr>
								<th>Σύνολο/Ποσοστό</th>
								<td class='last-row'>".$totalActive."/".$active."%</td>
								<td class='last-row'>".$totalInactive."/".$inactive."%</td>
								<td class='last-row'>".$total."</td>
							</tr>
						</table>
						<br />
						<img src='tmp/total_staff_pie.png' alt='total_staff_pie'></img>");
	
	$dompdf->setPaper('A4');
	
	$dompdf->render();
	
	$dompdf->stream("", array("Attachment" =>0));
	
?>