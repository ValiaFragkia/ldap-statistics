<?php
	// Include autoloader
	require_once "dompdf/autoload.inc.php";
	
	//start session
	session_start();
	
	use Dompdf\Dompdf;
	
	/* access DB */	
	$host = $_SESSION['host'];
	$username = $_SESSION['username'];
	$password = $_SESSION['password'];
	$dbName = $_SESSION['dbName'];
	
	@$db = new mysqli($host, $username, $password, $dbName);
	
	if (mysqli_connect_errno()) {
       echo '<p>Error: Could not connect to database.<br/>
       Please try again later.</p>';
       exit;
    }
	
    $query = "SELECT Biology, Chemistry, Csd, Econ, Fks, Hist_Arch, Materials, Math, Tem, 
					 Philology, Ptde, Ptpe, Social, Pol, Physics, Med, Psychology, Total, 
					 DATE_FORMAT(Ts, '%m-%d-%Y %H:%i') 
			  FROM AccountsVar ORDER BY Ts DESC LIMIT 1";
    $stmt = $db->prepare($query); 
    $stmt->execute();
    $stmt->store_result();
  
    $stmt->bind_result($biologyVar, $chemistryVar, $csdVar, $econVar, $fksVar, $histArchVar, 
					   $materialsVar, $mathVar, $temVar, $philologyVar, $ptdeVar, $ptpeVar, 
					   $socialVar, $polVar, $physicsVar, $medVar, $psychologyVar, $total, $ts);

    while($stmt->fetch()) {
		$biologyVar; 
		$chemistryVar; 
		$csdVar; 
		$econVar; 
		$fksVar; 
		$histArchVar; 
		$materialsVar; 
		$mathVar; 
		$temVar;
		$philologyVar; 
		$ptdeVar; 
		$ptpeVar; 
		$socialVar;  
		$polVar; 
		$physicsVar; 
		$medVar; 
		$psychologyVar;
		$total;
		$ts;
    }

    $stmt->free_result();
    $db->close();
	
	//create pdf
	$dompdf = new Dompdf();

	$dompdf->loadHtml("<head>
						<title>Στατιστικά Κεντρικής Υπηρεσίας Καταλόγου</title>
						<link href='css/stylesPdf.css' type='text/css' rel='stylesheet'>
					   </head><header class='header-class'>
						<meta charset='utf-8'>
						</header>
						<h2>Λογαριασμοί Var</h2>
						<table id='table_date'>
							<tr>
								<th>Ημερομηνία</th>
								<td id='cell'>".$ts."</td>
							</tr>
						</table>
						<table>
						<thead>
							<tr>
								<th>ΤΜΗΜΑ\Λογαριασμοί Var</th>
								<th>Var</th>
							</tr>
							</thead>
							<tbody>
								<tr class='odd'>
									<td>ΤΜΗΜΑ ΒΙΟΛΟΓΙΑΣ</td>
									<td>".$biologyVar."</td>
								</tr>
								<tr>
									<td>ΤΜΗΜΑ ΧΗΜΕΙΑΣ</td>
									<td>".$chemistryVar."</td>
								</tr>
								<tr class='odd'>
									<td>ΤΜΗΜΑ ΕΠΙΣΤΗΜΗΣ ΥΠΟΛΟΓΙΣΤΩΝ</td>
									<td>".$csdVar."</td>
								</tr>
								<tr'>
									<td>ΤΜΗΜΑ ΟΙΚΟΝΟΜΙΚΩΝ ΕΠΙΣΤΗΜΩΝ</td>
									<td>".$econVar."</td>
								</tr>
								<tr class='odd'>
									<td>ΤΜΗΜΑ ΦΙΛΟΣΟΦΙΚΩΝ ΚΑΙ ΚΟΙΝΩΝΙΚΩΝ ΣΠΟΥΔΩΝ</td>
									<td>".$fksVar."</td>
								</tr>
								<tr>
									<td>ΤΜΗΜΑ ΙΣΤΟΡΙΑΣ ΚΑΙ ΑΡΧΑΙΟΛΟΓΙΑΣ</td>
									<td>".$histArchVar."</td>
								</tr>
								<tr class='odd'>
									<td>ΤΜΗΜΑ ΕΠΙΣΤΗΜΗΣ ΚΑΙ ΤΕΧΝΟΛΟΓΙΑΣ ΥΛΙΚΩΝ</td>
									<td>".$materialsVar."</td>
								</tr>
								<tr>
									<td>ΤΜΗΜΑ ΜΑΘ.& ΕΦ.ΜΑΘ. - ΜΑΘΗΜΑΤΙΚΩΝ</td>
									<td>".$mathVar."</td>
								</tr>
								<tr class='odd'>
									<td>ΤΜΗΜΑ ΜΑΘ.& ΕΦ.ΜΑΘ. - ΕΦΑΡΜΟΣΜΕΝΩΝ ΜΑΘΗΜΑΤΙΚΩΝ</td>
									<td>".$temVar."</td>
								</tr>
								<tr>
									<td>ΤΜΗΜΑ ΦΙΛΟΛΟΓΙΑΣ</td>
									<td>".$philologyVar."</td>
								</tr>
								<tr class='odd'>
									<td>ΤΜΗΜΑ ΔΗΜΟΤΙΚΗΣ ΕΚΠΑΙΔΕΥΣΗΣ (ΠΑΙΔΑΓΩΓΙΚΟ)</td>
									<td>".$ptdeVar."</td>
								</tr>
								<tr>
									<td>ΤΜΗΜΑ ΠΡΟΣΧΟΛΙΚΗΣ ΕΚΠΑΙΔΕΥΣΗΣ (ΠΑΙΔΑΓΩΓΙΚΟ)</td>
									<td>".$ptpeVar."</td>
								</tr>
								<tr class='odd'>
									<td>ΤΜΗΜΑ ΚΟΙΝΩΝΙΟΛΟΓΙΑΣ</td>
									<td>".$socialVar."</td>
								</tr>
								<tr>
									<td>ΤΜΗΜΑ ΠΟΛΙΤΙΚΗΣ ΕΠΙΣΤΗΜΗΣ</td>
									<td>".$polVar."</td>
								</tr>
								<tr class='odd'>
									<td>ΤΜΗΜΑ ΦΥΣΙΚΗΣ</td>
									<td>".$physicsVar."</td>
								</tr>
								<tr>
									<td>ΤΜΗΜΑ ΙΑΤΡΙΚΗΣ</td>
									<td>".$medVar."</td>
								</tr>
								<tr class='odd'>
									<td>ΤΜΗΜΑ ΨΥΧΟΛΟΓΙΑΣ</td>
									<td>".$psychologyVar."</td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
									<td class='last-row'>Σύνολο Λογαριασμών Var</td>
									<td class='last-row'>".$total."</td>
								</tr>
							</tfoot>
						</table>
						<img src='tmp/var_pie.png' alt='var_pie'></img>");
	
	//$html = file_get_contents("hello.html");
//	$dompdf->loadHtml($html);
	
	$dompdf->setPaper('A4');
	
	$dompdf->render();
	
	$dompdf->stream("", array("Attachment" =>0));
?>