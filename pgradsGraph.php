<?php // content="text/plain; charset=utf-8"
	
	require("page.php");
	require_once ('jpgraph/src/jpgraph.php');
	require_once ('jpgraph/src/jpgraph_pie.php');
	require_once ('jpgraph/src/jpgraph_pie3d.php');

	//start session
	session_start();
	
	/* access DB */	
	$host = $_SESSION['host'];
	$username = $_SESSION['username'];
	$password = $_SESSION['password'];
	$dbName = $_SESSION['dbName'];
	
	@$db = new mysqli($host, $username, $password, $dbName);
	
	if (mysqli_connect_errno()) {
       echo '<p>Error: Could not connect to database.<br/>
       Please try again later.</p>';
       exit;
    }
	
    $query = "SELECT Biology, Chemistry, Csd, Econ, Fks, Hist_Arch, Materials, Math, Tem,
			  Philology, Ptde, Ptpe, Social, Pol, Physics, Med, Psychology, Total
			  FROM Pgrads ORDER BY Ts DESC LIMIT 1";
    $stmt = $db->prepare($query);  
    $stmt->execute();
    $stmt->store_result();
	
  $stmt->bind_result($biologyPgrads, $chemistryPgrads, $csdPgrads, $econPgrads, $fksPgrads, 
				     $histArchPgrads, $materialsPgrads, $mathPgrads, $temPgrads, 
					 $philologyPgrads, $ptdePgrads, $ptpePgrads, $socialPgrads, $polPgrads, 
					 $physicsPgrads, $medPgrads, $psychologyPgrads, $total);

    while($stmt->fetch()) {
		$biologyPgrads; 
		$chemistryPgrads; 
		$csdPgrads; 
		$econPgrads; 
		$fksPgrads; 
		$histArchPgrads; 
		$materialsPgrads; 
		$mathPgrads; 
	    $temPgrads;
		$philologyPgrads; 
		$ptdePgrads; 
		$ptpePgrads; 
		$socialPgrads;  
		$polPgrads; 
		$physicsPgrads; 
		$medPgrads; 
		$psychologyPgrads;
		$total;
    }

    $stmt->free_result();
    $db->close();
		
	//percentages for pgrads pie
	$biologyPerc = round(($biologyPgrads/$total)*100);
	$chemistryPerc = round(($chemistryPgrads/$total)*100); 
	$csdPerc = round(($csdPgrads/$total)*100); 
	$econPerc = round(($econPgrads/$total)*100); 
	$fksPerc = round(($fksPgrads/$total)*100); 
	$histArchPerc = round(($histArchPgrads/$total)*100); 
	$materialsPerc = round(($materialsPgrads/$total)*100); 
	$mathPerc = round(($mathPgrads/$total)*100); 
	$temPerc = round(($temPgrads/$total)*100); 
	$philologyPerc = round(($philologyPgrads/$total)*100); 
	$ptdePerc = round(($ptdePgrads/$total)*100); 
	$ptpePerc = round(($ptpePgrads/$total)*100); 
	$socialPerc = round(($socialPgrads/$total)*100); 
	$polPerc = round(($polPgrads/$total)*100); 
	$physicsPerc = round(($physicsPgrads/$total)*100); 
	$medPerc = round(($medPgrads/$total)*100); 
	$psychologyPerc = round(($psychologyPgrads/$total)*100); 
	
	// Some data
	$data = array($biologyPerc, $chemistryPerc, $csdPerc, $econPerc, $fksPerc, $histArchPerc,
				  $materialsPerc, $mathPerc, $temPerc, $philologyPerc, $ptdePerc, $ptpePerc, 
				  $socialPerc, $polPerc, $physicsPerc, $medPerc, $psychologyPerc);

	// Create the Pie Graph. 
	$graph = new PieGraph(660, 660);

	$theme_class= new UniversalTheme;
	$graph->SetTheme($theme_class);

	// Set A title for the plot
	$graph->title->Set("Μεταπτυχιακοί Φοιτητές Ανά Τμήμα");
	$graph->title->SetFont(FF_VERDANA,FS_BOLD,14);
	
	// Create
	$p1 = new PiePlot3D($data);
	$graph->Add($p1);

	$p1->ShowBorder();
	$p1->SetColor('black');
	$p1->SetSliceColors(array('#ff9446','#A02166','#2E8E2E','#560682', '#0b93b3', '#FFDB5A', 
							  '#8E8ED6', '#0A4800', '#0867B0', '#8006B0', '#34387B', '#73C567',
							  '#FF5306','#630014', '#045B86', '#9A2D2D', '#524DCD'));
	$p1->SetStartAngle(52);
	$p1->value->SetFont(FF_FONT1,FS_BOLD,5);
	$p1->value->SetColor('#FFFEF3');
	$p1->SetLabels($data,0.6);
	
	$p1->SetShadow();
	$p1->ExplodeAll();

	$p1->SetLegends(array("ΤΜΗΜΑ ΒΙΟΛΟΓΙΑΣ", "ΤΜΗΜΑ ΧΗΜΕΙΑΣ", "ΤΜΗΜΑ ΕΠΙΣΤ. ΥΠΟΛ.",
						  "ΤΜΗΜΑ ΟΙΚΟΝ. ΕΠΙΣΤΗΜΩΝ", "ΤΜΗΜΑ ΦΙΛ. & ΚΟΙN. ΣΠΟΥΔΩΝ",  
						  "ΤΜΗΜΑ ΙΣΤ. & ΑΡΧ.", "ΤΜΗΜΑ ΕΠΙΣΤ. & ΤΕΧΝ. ΥΛΙΚΩΝ", 
						  "ΤΜΗΜΑ ΜΑΘ.& ΕΦ. ΜΑΘ. - ΜΑΘ.", "ΤΜΗΜΑ ΜΑΘ.& ΕΦ. ΜΑΘ.-ΕΦ. ΜΑΘ.",
						  "ΤΜΗΜΑ ΦΙΛΟΛΟΓΙΑΣ", "ΤΜΗΜΑ ΔΗΜ. ΕΚΠ. (ΠΑΙΔΑΓ.)", 
						  "ΤΜΗΜΑ ΠΡΟΣΧ. ΕΚΠ. (ΠΑΙΔΑΓ.)", "ΤΜΗΜΑ ΚΟΙΝΩΝΙΟΛ.", 
						  "ΤΜΗΜΑ ΠΟΛΙΤΙΚΗΣ ΕΠΙΣΤΗΜΗΣ", "ΤΜΗΜΑ ΦΥΣΙΚΗΣ", "ΤΜΗΜΑ ΙΑΤΡΙΚΗΣ", 
						  "ΤΜΗΜΑ ΨΥΧΟΛΟΓΙΑΣ"));
	$graph->legend->SetAbsPos(0,600,'right','center');
	$graph->legend->SetColumns(3);

//	$graph->Stroke();

	$gdImgHandler = $graph->Stroke(_IMG_HANDLER);

	$fileName = "tmp/pgrads_pie.png";
	$graph->img->Stream($fileName);
 
	// Send it back to browser
	$graph->img->Headers();
	$graph->img->Stream();
?>