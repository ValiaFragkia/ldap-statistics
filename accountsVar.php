<?php
	/* Ldap is accessed in accountsVar.php and data is further stored in the DB.
	 AccountsVarGraph.php accesses the DB to read data */
	require("page.php");
	
	//start session
	session_start();
	
	/* function to count var */
	function countVar($school) {
		
		/* connect to ldap */
		$ldap_host = 'ds.uoc.gr';
		$ldap_port = 409;
	
		$ds = ldap_connect($ldap_host, $ldap_port)
				or die("Could not connect to LDAP server.");

		$bd = ldap_bind($ds) 
				or die ("System couldn't bind the connection!");
		
		$dn = "ou=var,ou=".$school.",dc=uoc,dc=gr";
	
		$filter = "(&(uid=*))";
	
		$search = ldap_search($ds, $dn, $filter);  //result
		$result = ldap_get_entries($ds, $search);	//get result
		$var = $result["count"];
		
		return $var;
	}

	/* access DB to read Timestamp*/	
        $host = $_SESSION['host'];
        $username = $_SESSION['username'];
        $password = $_SESSION['password'];
        $dbName = $_SESSION['dbName'];
	
	@$db = new mysqli($host, $username, $password, $dbName);
	
	if (mysqli_connect_errno()) {
       echo '<p>Error: Could not connect to database.<br/>
       Please try again later.</p>';
       exit;
    }
	
    $query = "SELECT UNIX_TIMESTAMP(Ts) FROM AccountsVar ORDER BY Ts DESC LIMIT 1";
    $stmt = $db->prepare($query); 
    $stmt->execute();
    $stmt->store_result();
  
    $stmt->bind_result($ts);

    while($stmt->fetch()) {
		$ts; 
    } 
	$stmt->free_result();
    $db->close();
	
	$nowunix = time();
	
	$diff = $nowunix - $ts;
	$diff = floor($diff/(60*60));
	
	if ($diff < 1) { //if Ts is less than 1 hour back
		/* access DB */	
		@$db = new mysqli($host, $username, $password, $dbName);
	
		if (mysqli_connect_errno()) {
			echo '<p>Error: Could not connect to database.<br/>
				     Please try again later.</p>';
			exit;
		}
	
		$query = "SELECT Biology, Chemistry, Csd, Econ, Fks, Hist_Arch, Materials, Math, Tem, 
				  Philology, Ptde, Ptpe, Social, Pol, Physics, Med, Psychology, Total
				  FROM AccountsVar ORDER BY Ts DESC LIMIT 1";
		$stmt = $db->prepare($query); 
		$stmt->execute();
		$stmt->store_result();
	
		$stmt->bind_result($biologyVar, $chemistryVar, $csdVar, $econVar, $fksVar, $histArchVar, 
						   $materialsVar, $mathVar, $temVar, $philologyVar, $ptdeVar, $ptpeVar, 
					       $socialVar, $polVar, $physicsVar, $medVar, $psychologyVar, $total);

		while($stmt->fetch()) {
			$biologyVar; 
			$chemistryVar; 
			$csdVar; 
			$econVar; 
			$fksVar; 
			$histArchVar; 
			$materialsVar; 
			$mathVar; 
			$temVar;
			$philologyVar; 
			$ptdeVar; 
			$ptpeVar; 
			$socialVar;  
			$polVar; 
			$physicsVar; 
			$medVar; 
			$psychologyVar;
			$total;
		}

		$stmt->free_result();
		$db->close();
		
	} else { //if Timestamp is older than 1 hour
		//1. biology
		$biologyVar = countVar("biology");
		
		//2. chemistry
		$chemistryVar = countVar("chemistry");
		
		//3. csd
		$csdVar = countVar("csd");
		
		//4. economics
		$econVar = countVar("econ");;
	
		//5. fks
		$fksVar = countVar("fks");;
	
		//6. history-archaeology
		$histArchVar = countVar("hist-arch");;
	
		//7. materials
		$materialsVar = countVar("materials");;
		
		//8. math
		$mathVar = countVar("math");
	
		//9. tem - Efarmosmenon Mathimatikon
		$temVar = countVar("tem");
		
		//10. philology
		$philologyVar = countVar("philology");
			
		//11. ptde - Dimotikis Ekpaideusis
		$ptdeVar = countVar("ptde");
		
		//12. ptpe - Prosxolikis Ekpaideusis
		$ptpeVar = countVar("ptpe");	

		//13. social
		$socialVar = countVar("social");
	
		//14. politikis epistimis
		$polVar = countVar("pol");
		
		//15. physics
		$physicsVar = countVar("physics");
	
		//16. medical
		$medVar = countVar("med");

		//17. psychology
		$psychologyVar = countVar("psychology");
		
		$total = $biologyVar + $chemistryVar + $csdVar + $econVar + $fksVar + $histArchVar + 
				 $materialsVar + $mathVar + $temVar + $philologyVar + $ptdeVar + $ptpeVar + $socialVar + 
				 $polVar + $physicsVar + $medVar + $psychologyVar;
	
		/* store data in DB */
		@$db = new mysqli($host, $username, $password, $dbName);

		if (mysqli_connect_errno()) {
			echo "<p>Error: Could not connect to database.<br/>
				     Please try again later.</p>";
			exit;
		}

		$query = "INSERT INTO AccountsVar VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 
				                                    ?, ?, ?, ?, ?, CURRENT_TIMESTAMP)";
		$stmt = $db->prepare($query);
		$stmt->bind_param('iiiiiiiiiiiiiiiiii', $biologyVar, $chemistryVar, $csdVar, $econVar, 
												$fksVar, $histArchVar, $materialsVar, $mathVar, 
												$temVar, $philologyVar, $ptdeVar, $ptpeVar, 
												$socialVar, $polVar, $physicsVar, $medVar, 
												$psychologyVar, $total);
		$stmt->execute();

		/* if ($stmt->affected_rows > 0) {
			echo  "<p>Stats inserted into the database.</p>";
		} else {
			echo "<p>An error has occurred.<br/>
                     Stats were not added.</p>";
		}*/
	
		$db->close();
	}
	
	$accountsVarPage = new Page();
	$accountsVarPage->table1 ="<p id='res'>Αποτελέσματα</p>
							   <p id='note'>Τα αποτελέσματα ανανεώνονται από τη Βάση Δεδομένων
									κάθε μία ώρα.</p>
						<table>
						<thead>
							<tr>
								<th>ΤΜΗΜΑ\Λογαριασμοί Var</th>
								<th>Var</th>
							</tr>
							</thead>
							<tbody>
								<tr class='odd'>
									<td>ΤΜΗΜΑ ΒΙΟΛΟΓΙΑΣ</td>
									<td>".$biologyVar."</td>
								</tr>
								<tr>
									<td>ΤΜΗΜΑ ΧΗΜΕΙΑΣ</td>
									<td>".$chemistryVar."</td>
								</tr>
								<tr class='odd'>
									<td>ΤΜΗΜΑ ΕΠΙΣΤΗΜΗΣ ΥΠΟΛΟΓΙΣΤΩΝ</td>
									<td>".$csdVar."</td>
								</tr>
								<tr'>
									<td>ΤΜΗΜΑ ΟΙΚΟΝΟΜΙΚΩΝ ΕΠΙΣΤΗΜΩΝ</td>
									<td>".$econVar."</td>
								</tr>
								<tr class='odd'>
									<td>ΤΜΗΜΑ ΦΙΛΟΣΟΦΙΚΩΝ ΚΑΙ ΚΟΙΝΩΝΙΚΩΝ ΣΠΟΥΔΩΝ</td>
									<td>".$fksVar."</td>
								</tr>
								<tr>
									<td>ΤΜΗΜΑ ΙΣΤΟΡΙΑΣ ΚΑΙ ΑΡΧΑΙΟΛΟΓΙΑΣ</td>
									<td>".$histArchVar."</td>
								</tr>
								<tr class='odd'>
									<td>ΤΜΗΜΑ ΕΠΙΣΤΗΜΗΣ ΚΑΙ ΤΕΧΝΟΛΟΓΙΑΣ ΥΛΙΚΩΝ</td>
									<td>".$materialsVar."</td>
								</tr>
								<tr>
									<td>ΤΜΗΜΑ ΜΑΘ.& ΕΦ.ΜΑΘ. - ΜΑΘΗΜΑΤΙΚΩΝ</td>
									<td>".$mathVar."</td>
								</tr>
								<tr class='odd'>
									<td>ΤΜΗΜΑ ΜΑΘ.& ΕΦ.ΜΑΘ. - ΕΦΑΡΜΟΣΜΕΝΩΝ ΜΑΘΗΜΑΤΙΚΩΝ</td>
									<td>".$temVar."</td>
								</tr>
								<tr>
									<td>ΤΜΗΜΑ ΦΙΛΟΛΟΓΙΑΣ</td>
									<td>".$philologyVar."</td>
								</tr>
								<tr class='odd'>
									<td>ΤΜΗΜΑ ΔΗΜΟΤΙΚΗΣ ΕΚΠΑΙΔΕΥΣΗΣ (ΠΑΙΔΑΓΩΓΙΚΟ)</td>
									<td>".$ptdeVar."</td>
								</tr>
								<tr>
									<td>ΤΜΗΜΑ ΠΡΟΣΧΟΛΙΚΗΣ ΕΚΠΑΙΔΕΥΣΗΣ (ΠΑΙΔΑΓΩΓΙΚΟ)</td>
									<td>".$ptpeVar."</td>
								</tr>
								<tr class='odd'>
									<td>ΤΜΗΜΑ ΚΟΙΝΩΝΙΟΛΟΓΙΑΣ</td>
									<td>".$socialVar."</td>
								</tr>
								<tr>
									<td>ΤΜΗΜΑ ΠΟΛΙΤΙΚΗΣ ΕΠΙΣΤΗΜΗΣ</td>
									<td>".$polVar."</td>
								</tr>
								<tr class='odd'>
									<td>ΤΜΗΜΑ ΦΥΣΙΚΗΣ</td>
									<td>".$physicsVar."</td>
								</tr>
								<tr>
									<td>ΤΜΗΜΑ ΙΑΤΡΙΚΗΣ</td>
									<td>".$medVar."</td>
								</tr>
								<tr class='odd'>
									<td>ΤΜΗΜΑ ΨΥΧΟΛΟΓΙΑΣ</td>
									<td>".$psychologyVar."</td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
									<td class='last-row'>Σύνολο Λογαριασμών Var</td>
									<td class='last-row'>".$total."</td>
								</tr>
							</tfoot>
						</table>";
	$accountsVarPage->graphs ="<div class='images'>
						<p id='img6' class='graphs'> <img src ='accountsVarGraph.php?a=1&b=2' alt='var_pie'> </p>
						</div>";
	$accountsVarPage->btn ="<form action='varPdf.php' method='get>
					 <p class='buttons'><input class='btn' type='submit' name='submit' value='Download pdf'></p>
					 </form>";
	$accountsVarPage->Display();
?>
