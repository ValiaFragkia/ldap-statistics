<?php
	// Include autoloader
	require_once "dompdf/autoload.inc.php";
	
	//start session
	session_start();
	
	use Dompdf\Dompdf;
	
	$host = $_SESSION['host'];
	$username = $_SESSION['username'];
	$password = $_SESSION['password'];
	$dbName = $_SESSION['dbName'];
	
	@$db = new mysqli($host, $username, $password, $dbName);
	
	if (mysqli_connect_errno()) {
		echo '<p>Error: Could not connect to database.<br/>
				 Please try again later.</p>';
		exit;
	}
	
	$query = "SELECT  TotalActive, Alumni, Total, 
					  DATE_FORMAT(Ts, '%m-%d-%Y %H:%i') 
			  FROM Ugrads ORDER BY Ts DESC LIMIT 1";
	$stmt = $db->prepare($query);  
	$stmt->execute();
	$stmt->store_result();
	
	$stmt->bind_result($totalActiveUgrads, $alumniUgrads, $totalUgrads, $ts);

	while($stmt->fetch()) {
		$totalActiveUgrads;
		$alumniUgrads;
		$totalUgrads;
		$ts;
	}

	$stmt->free_result();
	$db->close();
		
	/* access DB to read pgrads */	
	@$db = new mysqli($host, $username, $password, $dbName);
	
	if (mysqli_connect_errno()) {
		echo '<p>Error: Could not connect to database.<br/>
				 Please try again later.</p>';
		exit;
	}
	
	$query = "SELECT  TotalActive, Alumni, Total FROM Pgrads ORDER BY Ts DESC LIMIT 1";
	$stmt = $db->prepare($query);  
	$stmt->execute();
	$stmt->store_result();
	
	$stmt->bind_result($totalActivePgrads, $alumniPgrads, $totalPgrads);

	while($stmt->fetch()) {
		$totalActivePgrads;
		$alumniPgrads;
		$totalPgrads;
	}

	$stmt->free_result();
	$db->close();
		
	$total = $totalUgrads + $totalPgrads;
	$totalActive = $totalActiveUgrads + $totalActivePgrads;
	$totalAlumni = $alumniUgrads + $alumniPgrads;
	
	$activePerc = round(($totalActive/$total)*100);
	$alumniPerc = round(($totalAlumni/$total)*100);
	
	//create pdf
	$dompdf = new Dompdf();

	$dompdf->loadHtml("<head>
						<title>Στατιστικά Κεντρικής Υπηρεσίας Καταλόγου</title>
						<link href='css/stylesPdf.css' type='text/css' rel='stylesheet'>
					   </head><header class='header-class'>
						<meta charset='utf-8'>
						</header>
						<h2>Φοιτητές</h2>
						<table id='table_date'>
							<tr>
								<th>Ημερομηνία</th>
								<td id='cell'>".$ts."</td>
							</tr>
						</table>
						<table>
							<tr>
								<th>Ενεργοί - αναστολή σπουδών φοιτητές</th>
								<td class='bold-text'>".$totalActive."</td>
								<td class='bold-text'>".$activePerc."%</td>
							</tr>
							<tr>
								<th>Μη ενεργοί φοιτητές</th>
								<td class='bold-text'>".$totalAlumni."</td>
								<td class='bold-text'>".$alumniPerc."%</td>
							</tr>
							<tr>
								<th>Σύνολο φοιτητών</th>
								<td class='bold-text'>".$total."</td>
								<td class='bold-text'>100%</td>
							</tr>
						</table>
						<br />
						<br />
						<img src='tmp/ugrads_pie.png' alt='ugrads_pie'></img>
						<img src='tmp/pgrads_pie.png' alt='pgrads_pie'></img>");
	
	$dompdf->setPaper('A4');
	
	$dompdf->render();
	
	$dompdf->stream("", array("Attachment" =>0));
	
?>