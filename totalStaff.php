<?php // content="text/plain; charset=utf-8"
	
	require("page.php");
	require_once ('jpgraph/src/jpgraph.php');
	require_once ('jpgraph/src/jpgraph_pie.php');
	require_once ('jpgraph/src/jpgraph_pie3d.php');

	//start session
	session_start();
	
	/* access DB */	
	$host = $_SESSION['host'];
	$username = $_SESSION['username'];
	$password = $_SESSION['password'];
	$dbName = $_SESSION['dbName'];
	
	@$db = new mysqli($host, $username, $password, $dbName);
	
	if (mysqli_connect_errno()) {
       echo '<p>Error: Could not connect to database.<br/>
       Please try again later.</p>';
       exit;
    }
	
    $query = "SELECT ActiveTotal, InactiveTotal, Total FROM Staff ORDER BY Ts DESC LIMIT 1";
    $stmt = $db->prepare($query);
 //   $stmt->bind_param('i', $id);  
    $stmt->execute();
    $stmt->store_result();
  
    $stmt->bind_result($activeTotal, $inactiveTotal, $total);

    while($stmt->fetch()) {
      $activeTotal;
      $inactiveTotal;
	  $total;
    }

    $stmt->free_result();
    $db->close();
	
	//percentages for pie 1
	$activePerc = round(($activeTotal/$total)*100);
	$inactivePerc = round(($inactiveTotal/$total)*100);
	
	// Some data
	$data = array($activeTotal, $inactiveTotal);

	// Create the Pie Graph. 
	$graph = new PieGraph(500,350);

	$theme_class= new UniversalTheme;
	$graph->SetTheme($theme_class);

	// Set A title for the plot
	$graph->title->Set("Κατάσταση Λογαριασμών Προσωπικού");
	$graph->title->SetFont(FF_VERDANA,FS_BOLD,14);
	
	// Create
	$p1 = new PiePlot3D($data);
	$graph->Add($p1);

	$p1->ShowBorder();
	$p1->SetColor('black');
	$p1->SetSliceColors(array('#34387B','#A03451'));
	$p1->ExplodeSlice(1);
	$p1->value->SetFont(FF_FONT1,FS_BOLD,5);
	$p1->value->SetColor('#FFFEF3');
	$p1->SetLabels($data,0.5);

	$p1->SetLegends(array("Ενεργό Προσωπικό","Μη ενεργό Προσωπικό"));
	$graph->legend->SetAbsPos(0,330,'right','center');
	$graph->legend->SetColumns(1);

//	$graph->Stroke();

	$gdImgHandler = $graph->Stroke(_IMG_HANDLER);

	$fileName = "tmp/total_staff_pie.png";
	$graph->img->Stream($fileName);
 
	// Send it back to browser
	$graph->img->Headers();
	$graph->img->Stream();
?>